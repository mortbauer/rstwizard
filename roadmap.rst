Roadmap
#######

Mark 1
******
Mark 1 should be fully functional and compatible with sphinx with heavy
subclassing of sphinx and docutils stuff, but without all the clim bim of
sphinx, no need fo pickling and so on, but with XRef and more.

* subclass docutils.writers.latex2e.Writer

  add additionally needed visitors for nodes from sphinx

* subclass sphinx.environment.BuildEnvironment

  * provide additional method for direct publishing
  * use the custom writer


* subclass sphinx.application.Sphinx

  overwrite the _init_env to use the custom BuildEnvironment

Mark 2
******
Fine tweak the latex styles, add support for pluginbased styles, dropping a
stylesheet into the stylesheetpath should make the new style available. Add
nice titlepages as a sub item of a style.

Mark 3
******
Now you should know a lot about restructuredtext, docutils and sphinx so maybe
make a better lib, combining both with some new syntax, different header style,
different table style,... 
