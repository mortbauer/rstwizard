from setuptools import setup


setup(
    name = "rstwizard",
    version = "0.0.0",
    author = "Martin Ortbauer",
    author_email = "mortbauer@gmail.com",
    description = ("An app increasing rst to pdf,html,slides conversion speed."),
    license = "BSD",
    keywords = "documenting",
    url = "",
    packages=['rstwizard'],
    package_data={'rstwizard':['Makefile']},
    requires=[
        'argparse',
        'subprocess',
        'shutil'],
    provides='rstwizard',
    entry_points = {
        'console_scripts' : ['rstwizard = rstwizard.main:main','latexlog = rstwizard.parsetexlog:main_console']
        },
    long_description='',
    classifiers=[
        "Development Status :: Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)
