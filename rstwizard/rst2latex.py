#!/usr/bin/python

# Author: David Goodger, the Pygments team, GÃ¼nter Milde
# Date: $Date: $
# Copyright: This module has been placed in the public domain.

# This is a merge of the docutils_ `rst2latex` front end with an extension
# suggestion taken from the pygments_ documentation.

"""
A front end to docutils, producing LaTeX with syntax colouring using pygments
"""

try:
    import locale
    locale.setlocale(locale.LC_ALL, '')
except:
    pass

from docutils.core import publish_string,publish_cmdline,publish_file, default_description

description = ('Generates LaTeX documents from standalone reStructuredText '
               'sources. Uses `pygments` to colorize the content of'
               '"code-block" directives. Needs an adapted stylesheet'
               + default_description)

# Define a new directive `code-block` that uses the `pygments` source
# highlighter to render code in color.
#
# Code from the `pygments`_ documentation for `Using Pygments in ReST
# documents`_.

from docutils import nodes
from docutils.parsers.rst import directives
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import LatexFormatter

import pygments
pygments_formatter = LatexFormatter()

# plugins
from . import numfig

def pygments_directive(name, arguments, options, content, lineno,
                       content_offset, block_text, state, state_machine):
    try:
        lexer = get_lexer_by_name(arguments[0],encoding='utf-8')
    except ValueError:
        # no lexer found - use the text one instead of an exception
        lexer = get_lexer_by_name('text')
    hline = r'\vskip\medskipamount\leaders\vrule width \textwidth\vskip0.4pt \vskip\medskipamount \nointerlineskip'
    parsed = highlight(u'\n'.join(content), lexer, pygments_formatter)
    return [nodes.raw('', r'\begin{adjustwidth}{3em}{1em}' +hline + r'\footnotesize' + parsed + r'\normalsize' + hline + r'\end{adjustwidth}' , format='latex')]

pygments_directive.arguments = (1, 0, 1)
pygments_directive.content = 1
directives.register_directive('code-block', pygments_directive)

# Call the docutils publisher to render the input as latex::

#publish_cmdline(writer_name='latex', description=description)

def rst2latex(rstinput,titlepage,xelatex):
    if xelatex:
        settings ={'stylesheet':['pygments_default.sty'],
                'latex_preamble':'\\usepackage{unicode-math}\\setmathfont{xits-math.otf}\\usepackage{color}\n\\usepackage{fancyvrb}\n\\usepackage{changepage}',
                'output_encoding': 'utf-8','template':'titlepage.tex' if titlepage else 'default.tex'}
        return publish_string(rstinput,writer_name='xetex',settings_overrides=settings)
    else:
        settings ={'stylesheet':['pygments_default.sty'],
                'latex_preamble':'\\usepackage{color}\n\\usepackage{fancyvrb}\n\\usepackage{textcomp}\\usepackage{changepage}\\usepackage{undertilde}',
                'output_encoding': 'utf-8','template':'titlepage.tex' if titlepage else 'default.tex'}

        return publish_string(rstinput,writer_name='latex',settings_overrides=settings)


# .. _doctutile: http://docutils.sf.net/
# .. _pygments: http://pygments.org/
# .. _Using Pygments in ReST documents: http://pygments.org/docs/rstdirective/
