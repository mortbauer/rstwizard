# coding=utf-8
import re
import io
import os
import sys
import time
import json
import shutil
import debug
from jinja2 import Environment, FileSystemLoader
import codecs
import cPickle
import tempfile
import argparse
import logging
import colorama
import subprocess

from rstwizard.writer import MyBuilder

from docutils import nodes
from docutils.io import FileInput, NullOutput, FileOutput
from docutils.core import Publisher
from docutils.utils import Reporter
from docutils.parsers.rst import roles, directives
from docutils.parsers.rst.languages import en as english
from docutils.parsers.rst.directives.html import MetaBody
import sphinx
from sphinx.roles import XRefRole
from sphinx import addnodes
from sphinx.application import Sphinx,ENV_PICKLE_FILENAME, BUILTIN_BUILDERS,BUILTIN_DOMAINS,events,bold
from sphinx.errors import SphinxError
from sphinx.config import Config
from sphinx.errors import SphinxError, SphinxWarning, ExtensionError, \
     VersionRequirementError
from sphinx import environment
from sphinx.util import pycompat  # imported for side-effects
from sphinx.util.nodes import WarningStream
from sphinx.util.osutil import ustrftime,fs_encoding, SEP, copyfile
from sphinx.addnodes import pending_xref

class num_ref(pending_xref):
    pass

def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

def setup_latex_jinja_env(env):
    LATEX_SUBS = (
        (re.compile(r'\\'), r'\\textbackslash'),
        (re.compile(r'([{}_#%&$])'), r'\\\1'),
        (re.compile(r'~'), r'\~{}'),
        (re.compile(r'\^'), r'\^{}'),
        (re.compile(r'"'), r"''"),
        (re.compile(r'\.\.\.+'), r'\\ldots'),
    )

    def escape_tex(value):
        newval = value
        for pattern, replacement in LATEX_SUBS:
            newval = pattern.sub(replacement, newval)
        return newval

    env.block_start_string = '((*'
    env.block_end_string = '*))'
    env.variable_start_string = '((('
    env.variable_end_string = ')))'
    env.comment_start_string = '((='
    env.comment_end_string = '=))'
    env.filters['escape_tex'] = escape_tex
    return env

class ColorizingStreamHandler(logging.StreamHandler):
    # Courtesy http://plumberjack.blogspot.com/2010/12/colorizing-logging-output-in-terminals.html
    # Tweaked to use colorama for the coloring

    """
    Sets up a colorized logger, which is used ltscript
    """
    color_map = {
        logging.INFO: colorama.Fore.WHITE,
        logging.DEBUG: colorama.Style.DIM + colorama.Fore.CYAN,
        logging.WARNING: colorama.Fore.YELLOW,
        logging.ERROR: colorama.Fore.RED,
        logging.CRITICAL: colorama.Back.RED,
        logging.FATAL: colorama.Back.RED,
    }

    def __init__(self, stream, color_map=None):
        logging.StreamHandler.__init__(self,
                                    colorama.AnsiToWin32(stream).stream)
        if color_map is not None:
            self.color_map = color_map

    @property
    def is_tty(self):
        isatty = getattr(self.stream, 'isatty', None)
        return isatty and isatty()

    def format(self, record):
        message = logging.StreamHandler.format(self, record)
        if self.is_tty:
            # Don't colorize a traceback
            parts = message.split('\n', 1)
            parts[0] = self.colorize(parts[0], record)
            message = '\n'.join(parts)
        return message

    def colorize(self, message, record):
        try:
            return (self.color_map[record.levelno] + message +
                    colorama.Style.RESET_ALL)
        except KeyError:
            return message

class MyBuildEnvironment(environment.BuildEnvironment):
    def __init__(self, srcdir, doctreedir, config):
        environment.BuildEnvironment.__init__(self,srcdir,doctreedir,config)
        self.doctrees = {}

    def process_metadata(self, docname, doctree):
        """Process the docinfo part of the doctree as metadata.

        Keep processing minimal -- just return what docutils says.
        """
        self.metadata[docname] = md = {}
        try:
            docinfo = doctree[0]
        except IndexError:
            # probably an empty document
            return
        if docinfo.__class__ is not nodes.docinfo:
            # nothing to see here
            return
        for node in docinfo:
            # nodes are multiply inherited...
            if isinstance(node, nodes.authors):
                md['authors'] = ', '.join([author.astext() for author in node])
            elif isinstance(node, nodes.TextElement): # e.g. author
                md[node.__class__.__name__] = node.astext()
            else:
                name, body = node
                md[name.astext()] = body.astext()
        # titlepicture needs to get a good path
        if 'titlepicture' in md:
            pass
        del doctree[0]


    def get_doctree(self, docname,nopickle=False):
        """Read the doctree for a file from the cPickle and return it."""
        if not nopickle:
            doctree_filename = self.doc2path(docname, self.doctreedir, '.doctree')
            f = open(doctree_filename, 'rb')
            try:
                doctree = cPickle.load(f)
            finally:
                f.close()
            doctree.settings.env = self
            doctree.reporter = Reporter(self.doc2path(docname), 2, 5,
                                        stream=WarningStream(self._warnfunc))
        else:
            doctree = self.doctrees[docname]
        return doctree

    def read_doc(self, docname, src_path=None, save_parsed=True, app=None):
        """Parse a file and add/update inventory entries for the doctree.

        If srcpath is given, read from a different source file.
        """
        # remove all inventory entries for that file
        if app:
            app.emit('env-purge-doc', self, docname)

        self.clear_doc(docname)

        if src_path is None:
            src_path = self.doc2path(docname)

        self.temp_data['docname'] = docname
        # defaults to the global default, but can be re-set in a document
        self.temp_data['default_domain'] = \
            self.domains.get(self.config.primary_domain)

        self.settings['input_encoding'] = self.config.source_encoding
        self.settings['trim_footnote_reference_space'] = \
            self.config.trim_footnote_reference_space
        self.settings['gettext_compact'] = self.config.gettext_compact

        self.patch_lookup_functions()

        if self.config.default_role:
            role_fn, messages = roles.role(self.config.default_role, english,
                                           0, dummy_reporter)
            if role_fn:
                roles._roles[''] = role_fn
            else:
                self.warn(docname, 'default role %s not found' %
                          self.config.default_role)

        codecs.register_error('sphinx', self.warn_and_replace)

        class SphinxSourceClass(FileInput):
            def __init__(self_, *args, **kwds):
                # don't call sys.exit() on IOErrors
                kwds['handle_io_errors'] = False
                FileInput.__init__(self_, *args, **kwds)

            def decode(self_, data):
                if isinstance(data, unicode):
                    return data
                return data.decode(self_.encoding, 'sphinx')

            def read(self_):
                data = FileInput.read(self_)
                if app:
                    arg = [data]
                    app.emit('source-read', docname, arg)
                    data = arg[0]
                if self.config.rst_epilog:
                    data = data + '\n' + self.config.rst_epilog + '\n'
                if self.config.rst_prolog:
                    data = self.config.rst_prolog + '\n' + data
                return data

        # publish manually
        pub = Publisher(reader=environment.SphinxStandaloneReader(),
                        writer=environment.SphinxDummyWriter(),
                        source_class=SphinxSourceClass,
                        destination_class=NullOutput)
        pub.set_components(None, 'restructuredtext', None)
        pub.process_programmatic_settings(None, self.settings, None)
        pub.set_source(None, src_path.encode(fs_encoding))
        pub.set_destination(None, None)
        try:
            pub.publish()
            doctree = pub.document
        except UnicodeError, err:
            raise SphinxError(str(err))

        # post-processing
        self.filter_messages(doctree)
        self.process_dependencies(docname, doctree)
        self.process_images(docname, doctree)
        self.process_downloads(docname, doctree)
        self.process_metadata(docname, doctree)
        self.process_refonly_bullet_lists(docname, doctree)
        self.create_title_from(docname, doctree)
        self.note_indexentries_from(docname, doctree)
        self.note_citations_from(docname, doctree)
        self.build_toc_from(docname, doctree)
        for domain in self.domains.itervalues():
            domain.process_doc(self, docname, doctree)

        # allow extension-specific post-processing
        if app:
            app.emit('doctree-read', doctree)

        # store time of build, for outdated files detection
        # (Some filesystems have coarse timestamp resolution;
        # therefore time.time() can be older than filesystem's timestamp.
        # For example, FAT32 has 2sec timestamp resolution.)
        self.all_docs[docname] = max(
                time.time(), os.path.getmtime(self.doc2path(docname)))

        if self.versioning_condition:
            # get old doctree
            try:
                f = open(self.doc2path(docname,
                                       self.doctreedir, '.doctree'), 'rb')
                try:
                    old_doctree = cPickle.load(f)
                finally:
                    f.close()
            except EnvironmentError:
                old_doctree = None

            # add uids for versioning
            if old_doctree is None:
                list(add_uids(doctree, self.versioning_condition))
            else:
                list(merge_doctrees(
                    old_doctree, doctree, self.versioning_condition))

        # cleanup
        self.temp_data.clear()

        if save_parsed:
            # make it picklable
            doctree.reporter = None
            doctree.transformer = None
            doctree.settings.warning_stream = None
            doctree.settings.env = None
            doctree.settings.record_dependencies = None
            for metanode in doctree.traverse(MetaBody.meta):
                # docutils' meta nodes aren't picklable because the class is nested
                metanode.__class__ = addnodes.meta

            # save the parsed doctree
            doctree_filename = self.doc2path(docname, self.doctreedir,
                                             '.doctree')
            dirname = os.path.dirname(doctree_filename)
            if not os.path.isdir(dirname):
                os.makedirs(dirname)
            f = open(doctree_filename, 'wb')
            try:
                cPickle.dump(doctree, f, cPickle.HIGHEST_PROTOCOL)
            finally:
                f.close()
        else:
            self.doctrees[docname] = doctree

    # utilities to use while reading a document
    def update(self, config, srcdir, doctreedir, app=None, nopickle=False):
        """(Re-)read all files new or changed since last update.

        Returns a summary, the total count of documents to reread and an
        iterator that yields docnames as it processes them.  Store all
        environment docnames in the canonical format (ie using SEP as a
        separator in place of os.path.sep).
        """
        config_changed = False
        if self.config is None:
            msg = '[new config] '
            config_changed = True
        else:
            # check if a config value was changed that affects how
            # doctrees are read
            for key, descr in config.values.iteritems():
                if descr[1] != 'env':
                    continue
                if self.config[key] != config[key]:
                    msg = '[config changed] '
                    config_changed = True
                    break
            else:
                msg = ''
            # this value is not covered by the above loop because it is handled
            # specially by the config class
            if self.config.extensions != config.extensions:
                msg = '[extensions changed] '
                config_changed = True
        # the source and doctree directories may have been relocated
        self.srcdir = srcdir
        self.doctreedir = doctreedir
        self.find_files(config)
        self.config = config

        added, changed, removed = self.get_outdated_files(config_changed)

        # allow user intervention as well
        for docs in app.emit('env-get-outdated', self, added, changed, removed):
            changed.update(set(docs) & self.found_docs)

        # if files were added or removed, all documents with globbed toctrees
        # must be reread
        if added or removed:
            # ... but not those that already were removed
            changed.update(self.glob_toctrees & self.found_docs)

        msg += '%s added, %s changed, %s removed' % (len(added), len(changed),
                                                     len(removed))

        def update_generator():
            self.app = app
            # clear all files no longer present
            for docname in removed:
                if app:
                    app.emit('env-purge-doc', self, docname)
                self.clear_doc(docname)

            # read all new and changed files
            for docname in sorted(added | changed):
                yield docname
                doctree = self.read_doc(docname, app=app,save_parsed= not nopickle)

            if config.master_doc not in self.all_docs:
                self.warn(None, 'master file %s not found' %
                          self.doc2path(config.master_doc))

            if app:
                app.emit('env-updated', self)

            self.app = None

        return msg, len(added | changed), update_generator()
    def publish(self, docname, src_path=None, save_parsed=True, app=None):
        """Parse a file and add/update inventory entries for the doctree.

        If srcpath is given, read from a different source file.
        """
        # remove all inventory entries for that file
        if app:
            app.emit('env-purge-doc', self, docname)

        self.clear_doc(docname)

        if src_path is None:
            src_path = self.doc2path(docname)

        self.temp_data['docname'] = docname
        # defaults to the global default, but can be re-set in a document
        self.temp_data['default_domain'] = \
            self.domains.get(self.config.primary_domain)

        self.settings['input_encoding'] = self.config.source_encoding
        self.settings['trim_footnote_reference_space'] = \
            self.config.trim_footnote_reference_space
        self.settings['gettext_compact'] = self.config.gettext_compact

        self.patch_lookup_functions()

        if self.config.default_role:
            role_fn, messages = roles.role(self.config.default_role, english,
                                           0, dummy_reporter)
            if role_fn:
                roles._roles[''] = role_fn
            else:
                self.warn(docname, 'default role %s not found' %
                          self.config.default_role)

        codecs.register_error('sphinx', self.warn_and_replace)

        class SphinxSourceClass(FileInput):
            def __init__(self_, *args, **kwds):
                # don't call sys.exit() on IOErrors
                kwds['handle_io_errors'] = False
                FileInput.__init__(self_, *args, **kwds)

            def decode(self_, data):
                if isinstance(data, unicode):
                    return data
                return data.decode(self_.encoding, 'sphinx')

            def read(self_):
                data = FileInput.read(self_)
                if app:
                    arg = [data]
                    app.emit('source-read', docname, arg)
                    data = arg[0]
                if self.config.rst_epilog:
                    data = data + '\n' + self.config.rst_epilog + '\n'
                if self.config.rst_prolog:
                    data = self.config.rst_prolog + '\n' + data
                return data

        # publish manually
        self.publisher = pub = Publisher(reader=SphinxStandaloneReader(),
                        writer=SphinxDummyWriter(),
                        source_class=SphinxSourceClass,
                        destination_class=NullOutput)
        pub.set_components(None, 'restructuredtext', None)
        pub.process_programmatic_settings(None, self.settings, None)
        pub.set_source(None, src_path.encode(fs_encoding))
        pub.set_destination(None, None)
        try:
            pub.publish()
            doctree = pub.document
        except UnicodeError as err:
            raise SphinxError(str(err))

        # post-processing
        self.filter_messages(doctree)
        self.process_dependencies(docname, doctree)
        self.process_images(docname, doctree)
        self.process_downloads(docname, doctree)
        self.process_metadata(docname, doctree)
        self.process_refonly_bullet_lists(docname, doctree)
        self.create_title_from(docname, doctree)
        self.note_indexentries_from(docname, doctree)
        self.note_citations_from(docname, doctree)
        self.build_toc_from(docname, doctree)
        for domain in self.domains.itervalues():
            domain.process_doc(self, docname, doctree)

        # allow extension-specific post-processing
        if app:
            app.emit('doctree-read', doctree)

        # cleanup
        self.temp_data.clear()

        return doctree

    # utilities to use while reading a document

class MySphinx(Sphinx):

    def _log(self, message, wfile, nonl=False):
        if type(message) == unicode:
            message = str(message)
        try:
            wfile.write(message)
        except UnicodeEncodeError:
            encoding = getattr(wfile, 'encoding', 'ascii') or 'ascii'
            wfile.write(message.encode(encoding, 'replace'))
        if not nonl:
            wfile.write('\n')
        if hasattr(wfile, 'flush'):
            wfile.flush()

    def _init_env(self, freshenv):
        if freshenv:
            self.env = MyBuildEnvironment(self.srcdir, self.doctreedir,
                                        self.config)
            self.env.find_files(self.config)
            for domain in self.domains.keys():
                self.env.domains[domain] = self.domains[domain](self.env)
        else:
            try:
                self.info('loading pickled environment... ', nonl=True)
                self.env = MyBuildEnvironment.frompickle(self.config,
                    os.path.join(self.doctreedir, ENV_PICKLE_FILENAME))
                self.env.domains = {}
                for domain in self.domains.keys():
                    # this can raise if the data version doesn't fit
                    self.env.domains[domain] = self.domains[domain](self.env)
                self.info('done')
            except Exception, err:
                if type(err) is IOError and err.errno == os.errno.ENOENT:
                    self.info('not yet created')
                else:
                    self.info('failed: %s' % err)
                return self._init_env(freshenv=True)

        self.env.set_warnfunc(self.warn)

class MyFileSystemLoader(object):
    def __init__(self,searchpath):
        self.searchpath = searchpath

    def list_templates(self):
        try:
            return os.listdir(self.searchpath)
        except OSError as e:
            raise e

    def llist_templates(self):
        return [os.path.splitext(x)[0] for x in
                self.list_templates()]

    def get(self,template):
        try:
            if os.path.isfile(os.path.join(self.searchpath,template)):
                return os.path.join(self.searchpath,template)
        except OSError as e:
            raise e

class SphinxFrontend(object):

    def __init__(self,args):
        self.parser = self._make_parser()
        self.parse_args(args)
        self.logger = logging.getLogger('rstwizard')
        formatter = logging.Formatter('%(levelname)s %(name)s: %(message)s')
        handler = ColorizingStreamHandler(sys.stderr)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        if self.options.debug:
            self.logger.setLevel('DEBUG')
        else:
            self.logger.setLevel(getattr(logging,self.options.loglevel,'INFO'))
        self._init_styleloader()
        self.logger.info('SphinxFrontend initialized')

    def _init_styleloader(self):
        try:
            titlepage_env = Environment(
                    loader=FileSystemLoader(
                        os.path.abspath(self.options.titlepage_path)))
            self.titlepage_env = setup_latex_jinja_env(titlepage_env)
        except OSError as e:
            if e.errno == os.errno.ENOENT:
                print('couldn\'t load titlestyle from "{0}"'.format())

        try:
            styles_env = Environment(
                    loader=FileSystemLoader(
                        os.path.abspath(self.options.styles_path)))
            self.styles_env = setup_latex_jinja_env(styles_env)
        except OSError as e:
            if e.errno == os.errno.ENOENT:
                print('couldn\'t load style from "{0}"'.format())
        try:
            wrapperclass_env = Environment(
                    loader=FileSystemLoader(
                        os.path.abspath(self.options.wrapperclass_path)))
            self.wrapperclass_env = setup_latex_jinja_env(wrapperclass_env)
        except OSError as e:
            if e.errno == os.errno.ENOENT:
                print('couldn\'t load wrapperclass from "{0}"'.format())
        try:
            self.logos_env = MyFileSystemLoader(
                os.path.abspath(self.options.logos_path))
        except OSError as e:
            if e.errno == os.errno.ENOENT:
                print('couldn\'t load logos from "{0}"'.format())

    def wrapperclass_info(self):
        """ prints info on availabel wrapperclasses """
        from jinja2 import meta
        if self.options.wrapperclass:
            try:
                template_source = self.wrapperclass_env.loader.get_source(
                        self.wrapperclass_env,self.options.wrapperclass+'.cls')[0]
                parsed_content = self.wrapperclass_env.parse(template_source)
                variables = meta.find_undeclared_variables(parsed_content)
                print('\ntemplate "{0}" has following variables:\n\t{1}'.
                        format(self.options.wrapperclass,variables))
            except:
                print('could\'t find template "{0}"'.
                        format(self.options.wrapperclass))
        else:
            wrapperclasses = []
            for searchpath in self.wrapperclass_env.loader.searchpath:
                wrapperclasses.extend([os.path.splitext(x)[0] for x in
                        os.listdir(searchpath)])
            print('\navailable wrapperclasses are:\n {0}'.format(wrapperclasses))

    def styles_info(self):
        """ prints info on availabel wrapperclasses """
        from jinja2 import meta
        if self.options.style:
            try:
                template_source = self.styles_env.loader.get_source(
                        self.styles_env,self.options.style+'.sty')[0]
                parsed_content = self.styles_env.parse(template_source)
                variables = meta.find_undeclared_variables(parsed_content)
                print('\ntemplate "{0}" has following variables:\n\t{1}'.
                        format(self.options.style,variables))
            except:
                print('could\'t find template "{0}"'.
                        format(self.options.style))
        else:
            style = []
            for searchpath in self.styles_env.loader.searchpath:
                style.extend([os.path.splitext(x)[0] for x in
                        os.listdir(searchpath)])
            print('\navailable styles are:\n {0}'.format(style))

    def _make_parser(self):
        parser = argparse.ArgumentParser('rstwizard',add_help=False)
        parser.add_argument('--debug',action='store_true',default=False)
        parser.add_argument('--loglevel',default='WARNING')
        subparser = parser.add_subparsers(dest='action')


        # for the actual processing
        process = subparser.add_parser('latex',add_help=False)
        process.add_argument('input')
        process.add_argument('output')
        process.add_argument('-b','--builddir',
                help='build directory, without it pickling is useless')
        process.add_argument('-c','--wrapperclass',default='tu')
        process.add_argument('-t','--titlestyle',default='tu')
        process.add_argument('-s','--style',default='natural')
        process.add_argument('-d','--docclass',default='article')
        process.add_argument('-l','--latex-elements',default='{}')
        process.add_argument('-o','--overrides',default='{}')
        process.add_argument('-a','--write-all')
        process.add_argument('--nopickle',action='store_true')

        pdf = subparser.add_parser('pdf',parents=[process])
        pdf.add_argument('--draftmode',action='store_true')

        # to get information over the availabel styles and classes
        liststyles = subparser.add_parser('styleinfo')
        liststyles.add_argument('style',nargs='?')
        listclasses = subparser.add_parser('wrapperclassinfo')
        listclasses.add_argument('wrapperclass',nargs='?')
        listlogos = subparser.add_parser('listlogos')


        parser.add_argument('--conf',default=None)
        parser.add_argument('--titlepage-path',
                default=os.path.join(os.getenv('HOME'),'.config','sphinx','titlepages'))
        parser.add_argument('--styles-path',
                default=os.path.join(os.getenv('HOME'),'.config','sphinx','styles'))
        parser.add_argument('--wrapperclass-path',
                default=os.path.join(os.getenv('HOME'),'.config','sphinx','classes'))
        parser.add_argument('--logos-path',
                default=os.path.join(os.getenv('HOME'),'.config','sphinx','logos'))

        parser.add_argument('--builder',default='mylatex')
        parser.add_argument('--extensions',
                default=['sphinx.ext.mathjax','rstwizard.ext.subfig'],nargs='*')

        parser.add_argument('--author',default='')
        parser.add_argument('--title',default='')
        parser.add_argument('--logo',default=None)
        parser.add_argument('--options',default='{}')

        return parser

    def _setup_sphinx(self):
        options = self.options
        if options.input:
            options.inputpath = os.path.abspath(options.input)
            # get in/out directory
            #options.srcdir = os.path.abspath('.')
            options.srcdir = os.path.split(options.inputpath)[0]
            # get the filenames without extension
            options.inputname = os.path.split(os.path.splitext(options.input)[0])[1]
        # get configdir
        if options.conf:
            options.confdir = os.path.split(options.conf)[0]
        else:
            options.confdir = None
        # outputdir
        if options.output:
            options.outputpath = os.path.abspath(options.output)
            options.outputname = os.path.split(os.path.splitext(options.output)[0])[1]
            options.outdir = os.path.split(options.outputpath)[0]
            # create outputdir if not already
            try:
                os.mkdir(options.outdir)
            except OSError as e:
                if e.errno == os.errno.EEXIST:
                    pass

        # builddir
        if not options.builddir:
            self.tempdir = options.builddir=tempfile.mkdtemp(suffix='rstwizard')
        else:
            self.tempdir = None
            try:
                os.mkdir(options.builddir)
            except OSError as e:
                if e.errno == os.errno.EEXIST:
                    pass
        # setup latex_elements dict
        latex_elements = {}
        latex_elements['titlestyle'] = options.titlestyle
        latex_elements['wrapperclass'] = options.wrapperclass
        latex_elements['style'] = options.style
        latex_elements['docclass'] = options.docclass
        try:
            latex_elements.update(eval(options.latex_elements))
        except:
            print('latex_elements needs to be a valid python dict')
        self.latex_elements = latex_elements
        try:
            self.overrides = eval(options.overrides)
        except:
            print('overrides needs to be a valid python dict')
            self.overrides = {}


        confoverrides = {'extensions':set(
            #['sphinx.ext.mathjax']+self.options.extensions),
            ['sphinx.ext.mathjax','rstwizard.ext.bibtex','rstwizard.ext.subfigure']+
            self.options.extensions),
        'latex_documents':([options.inputname,options.outputname+'.tex',
            options.title,options.author,options.docclass],),
        'project':options.title,
        'copyright':options.author,
        'pygments_style': 'colorful',
        'latex_elements':self.latex_elements,
        'master_doc':options.inputname}

        self.overrides.update(confoverrides)

        # not very nice hack but what choice do i have
        BUILTIN_BUILDERS[MyBuilder.name] = MyBuilder

        # setup sphinx status
        self.sphinxstatus = status = io.BytesIO()
        self.sphinxwarnings = warnings = io.BytesIO()
        try:
            app = MySphinx(options.srcdir,options.confdir,options.builddir,
                    options.builddir,options.builder,confoverrides=self.overrides,
                    status=status,warning=warnings,freshenv=options.nopickle,
                    warningiserror=False,tags=None,verbosity=0)
        except Exception as e:
            self.logger.exception('failed to setup sphinx: {0}'.format(e))
            return False
        app.builder.titlepage_env = self.titlepage_env
        app.builder.styles_env = self.styles_env
        app.builder.wrapperclass_env = self.wrapperclass_env
        app.builder.logos_env = self.logos_env
        app.builder.nopickle = self.options.nopickle
        self.app = app
        app.add_role('num', XRefRole(lowercase=True, innernodeclass=nodes.emphasis,
                            warn_dangling=True,nodeclass=num_ref))
                            #warn_dangling=True,nodeclass=num_ref,refdomain='std'))
        self.logger.info('sphinx successfully setuped')
        return True

    def parse_args(self,args):
        self.options = options = self.parser.parse_args(args)

    def make_latex(self):
        filenames = [os.path.splitext(self.options.input)[0]]
        #try:
        self.app.build(self.options.write_all, [])
        if self.sphinxstatus.tell():
            self.sphinxstatus.seek(0)
            self.logger.info('sphinx messages:\n\n{0}'.format(self.sphinxstatus.read()))
        if self.sphinxwarnings.tell():
            self.sphinxwarnings.seek(0)
            self.logger.warn('sphinx warnings:\n\n{0}'.format(self.sphinxwarnings.read()))
        self.logger.info('successfully created latex document "{0}"'
                .format(self.options.outputname+'.tex'))
        return True
        #except Exception as e:
            #self.logger.exception('latex creation failed because of: {0}'
                    #.format(e))

    def parse_latexlog(self):
        try:
            returncode = subprocess.call(['pplatex','-i',
                self.options.outputname+'.log'],
                cwd=self.options.builddir)
        except OSError as e:
            if e.errno == os.errno.ENOENT:
                if not which('pplatex'):
                    self.logger.warn('"pplatex" needs to be installed for log parsing')
                else:
                    self.logger.error("log file parsing failed unexpected")
        except Exception as e:
            self.logger.error("log file parsing failed: {0}".format(e))


    def make_pdf(self):
        latexopt = {'interaction':'batchmode',
                'draftmode':self.options.draftmode}
        latexoptions = '-latexoption="'
        for key in latexopt:
            if latexopt[key] == True:
                latexoptions = '-'.join((latexoptions,key))
            elif latexopt[key]:
                latexoptions = '-'.join((latexoptions,'='.join((key,latexopt[key]))))
        latexoptions += '"'
        try:
            returncode = subprocess.call(['latexmk',latexoptions,'--pdf',
                self.options.outputname+'.tex'],cwd=self.options.builddir,
                stdout=open(os.devnull, 'wb'),stderr=open(os.devnull, 'wb'))
            if returncode and os.path.isfile(os.path.join(
                self.options.builddir,self.options.outputname+'.pdf')):
                self.logger.warn('pdflatex errors:\n')
                self.parse_latexlog()
                return True
            elif returncode:
                self.logger.warn('no pdf created, pdflatex errors:\n')
                self.parse_latexlog()
            else:
                return True

        except OSError as e:
            if e.errno == os.errno.ENOENT:
                if not which('latexmk'):
                    self.logger.warn('"latexmk" needs to be installed for log parsing')


    def copy_file(self,ext):
        options = self.options
        shutil.copyfile(
                os.path.join(options.builddir,options.outputname + ext),
                os.path.join(options.outdir,options.outputname + ext))
        return True

    def make_clean(self):
        if self.tempdir:
            shutil.rmtree(self.tempdir)

    def main(self):
        if self.options.action in ['latex','pdf']:
            t0 = time.time()
            if not self._setup_sphinx():
                sys.exit(1)
            tsetup = time.time()
            if not self.make_latex():
                sys.exit(1)
            tlatex = time.time()
        elif self.options.action == 'styleinfo':
            self.styles_info()
        elif self.options.action == 'wrapperclassinfo':
            self.wrapperclass_info()
        elif self.options.action == 'listlogos':
            for logo in self.logos_env.llist_templates():
                print(logo)

        if self.options.action in ['latex']:
            if not self.copy_file('.tex'):
                sys.exit(1)
            if not self.make_clean():
                sys.exit(1)
        elif self.options.action in ['pdf']:
            if not self.make_pdf():
                sys.exit(1)
            tpdf = time.time()
            #print('time for setup: {0}\ntime for latex: {1}\ntime for pdf: {2}'.
                    #format(tsetup-t0,tlatex-tsetup,tpdf-tlatex))
            if not self.copy_file('.pdf'):
                sys.exit(1)
            if not self.make_clean():
                sys.exit(1)


