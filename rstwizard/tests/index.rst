:subject: Fatigue of Structures 317.510
:subtitle: FEMFAT and Manual Calculations
:institute: E317 Institut für Leichtbau und Struktur-Biomechanik
:supervisor: Prof. Dipl.-Ing. Dr.techn. Heinz Pettermann
:date: 14 th of February 2013
:author: Martin Ortbauer
:logo: tuwien
:titlepicture: _figures/sigmaxy-L1.png

Tutorial for Fatigue of Structures
##################################

.. math:: \frac{2}{

.. toctree::
    
    inc

Task Description
****************

A thin plate with a hole in the middle is under cyclic loading. There should be
made safe life predictions, both manually and numerically. The software which
should be used for the numerical part is FEMFAT, a proprietary software
developed by Magna figure :num:`sncurve`.

Test Section
============
ksfjwkf

More Test Sections
------------------
dfdfgf

.. _geometry:

Geometry, Material and Loads
****************************
The plate has a length and width of 100mm, and a thickness of 1mm. It has
further a central hole with a diameter of 10mm. It's material is low carbon
steel (SAE 1015, St 37). The S-N curve for SAE 1015 for a load coefficient R=-1 is in :

.. _sncurve:

.. figure:: _figures/sn-curve-st37-R1.png
    :width: 60%

    S-N curve for the low carbon steel SAE 1015, with R=-1

On the plate operate three different unit load cases which have to be combined
accordingly in each task.

1. uni-axial horizontal tension 
2. uni-axial vertical tension
3. pressure int the hole :ref:`geometry`

Stress Data
***********
The stress data for this unit load cases was already calculated through a FEM
Program and stored in database which FEMFAT is able to read. The pictures shown
below just always show one quater of the plate, since the two symmetries were
used to minimize calculation.

.. figure:: _figures/fem-model.png
    :width: 60%

    Finite Element Model used to calculate the unit load cases in Abaqus.

.. _loadcase1xx:

.. figure:: _figures/sigmaxx-L1.png
    :width: 60%

    :math:`\sigma_{xx}` stress field for load case 1

.. _loadcase1yy:

.. figure:: _figures/sigmayy-L1.png
    :width: 60%

    :math:`\sigma_{yy}` stress field for load case 1

.. _loadcase1xy:

.. figure:: _figures/sigmaxy-L1.png
    :width: 60%

    :math:`\sigma_{xy}` stress field for load case 1

Task 1
******
In task 1 only the load case 1 is applied on the plate. With classical manual
tools, the load amplitude, which leads to a predicted life of 200 000 cycles
under the given load case should be calculated. 

We have given therefore following data:

.. math::

    R = -1
    N = 2e5 

From figure 3 we get the maximum stress in x-axes, which
are :math:`\sigma_{xx\_max} = 3.01 MPa`. These are much bigger then the
stresses in y-axes or the shear stresses which are therefore neglected.

From the s-n curve for our material, figure 1, we get a maximum
allowable stress for 200 000 load cycles of :math:`\sigma_a = 195 MPa`. This
leads to a load amplitude of:

.. math::

    \frac{\sigma_a}{\sigma_{xx\_max}} = 65

So the load amplitude for a stress life of 200 000 cycles is 65.

Task 2
******
The results of task 1 should be validate by use of the ``Basic`` module of
``FEMFAT``. The calculations should be further carried out two times, ones with
the influence of the stress gradient and once without.

Basically the following steps are needed to get the result in FEMFAT:

1. define stress data
2. define load case with number of cycles
3. define a material
4. set influence factors
5. calculate and read results

The direction of the calculation is somewhat different than for manual
calculation, as you cannot directly calculate the load factor which yields a
damage value of one under the given number of cycles, but you have to specify
some load amplitude and then get the damage value for the given number of
cycles. The results found for load case 1 with 200 000 under a load factor of
65, as given from the manual calculation is 1.114, without consideration of the
stress gradient. If we let the program let the stress gradient into account it
yields a damage value of 0.232, which is significantly lower.

The differences in the results by calculation with and without stress gradient
are huge, and confused me a lot as the stress gradient is used by default.

.. table::

    ======================================== =========
    damage value for N=200000,A=65
    --------------------------------------------------
    without stress gradient                     1.114
    with stress gradient                        0.232
    ======================================== =========
