
.. code-block:: python

	from pylab import log,exp
	
	#summary of chunk
	
	#summary of chunk


:title: UNIQUAC Methode
:date: 24\. Jänner 2013

Für das System Ethanol (1) und Wasser (2) soll bei einer Temperatur von 70°C
und einer Ethanol Konzentration von x\ :sub:`1` = 0.252 in der Flüssigphase mit
Hilfe der UNIQUAC Methode gerechnet werden. Für UNIFAC Methode siehe [SVNA]_ im
Anhang.

gegebene Parameter:


.. code-block:: python

	Tref = 70 + 273.15
	x_1 = 0.252
	x_2 = 1 - x_1
	delta_u_12 = -21.42
	delta_u_21 = 175.47
	r_1 = 2.1055
	r_2 = 0.92
	q_1 = 1.972
	q_2 = 1.4
	p_sat_1 = 72.3e3
	p_sat_2 = 31.09e3
	v_1 = 61.95e-6
	v_2 = 18.42e-6
	B_11 = -1100e-6
	b_12 = -850e-6
	B_22 = -650e-6
	Rm = 8.31433
	
	#summary of chunk
	
	Tref = 343.15
	q_2 = 1.4
	r_2 = 0.92
	B_11 = -0.0011
	Rm = 8.3143
	q_1 = 1.972
	p_sat_2 = 31090
	B_22 = -0.00065
	b_12 = -0.00085
	r_1 = 2.1055
	delta_u_12 = -21.42
	p_sat_1 = 72300
	delta_u_21 = 175.47
	v_2 = 1.842e-05
	x_1 = 0.252
	x_2 = 0.748
	v_1 = 6.195e-05
	#summary of chunk


a) Der System Gesamtdruck ist gesucht
*************************************

Der Gesamtdruck des Systems kann als Summe seiner Partialdrücke berechnet werden:

.. math:: p = p_1 + p_2 

Da über die UNIQUAC Methode die Aktivitätskoeffizienten und die
Fugazitätskoeffizienten berechnet werden sollen, können die Partialdrücke dann
über diese berechnet werden, es ergibt sich:

.. math:: p = x_1 \varphi_1 \gamma_1 p_1^{sat} + (1 -x_1) \varphi_2 \gamma_2 p_2^{sat}

Berechnung der Koeffizienten für die UNIFAC Methode
===========================================================

.. math:: \phi_i = \frac{x_i r_i}{\sum x_j r_j}


.. code-block:: python

	phi_1 = x_1 * r_1 /(x_1 * r_1 + x_2 * r_2)
	theta_1 = x_1 * q_1 /(x_1 * q_1 + x_2 * q_2)
	tau_12 = exp(-(delta_u_12 - 0)/(Rm*Tref))
	
	#summary of chunk
	
	theta_1 = 0.32182
	tau_12 = 1.0075
	phi_1 = 0.43535
	#summary of chunk


.. code-block:: python

	phi_2 = x_2 * r_2 /(x_2 * r_2 + x_2 * r_2)
	theta_2 = x_2 * q_2 /(x_2 * q_2 + x_2 * q_2)
	tau_21 = exp(-(delta_u_21 - 0)/(Rm*Tref))
	
	#summary of chunk
	
	theta_2 = 0.5
	tau_21 = 0.94035
	phi_2 = 0.5
	#summary of chunk


Berchnung des residualen und kombinatorischen Terms
===================================================
Nach der UNIQUAC Methode setzten sich die Aktivitätskoeffizienten aus einen
kombinatorischen und einem residualen Term zusammen zu:

.. math:: \ln\gamma_i = \ln\gamma_i^C + \ln\gamma_i^R

wobei sich diese durch folgende Gleichungen berechnen lassen:

.. math:: \ln\gamma_i^C = 1-J_i +\ln J_i -5 q_i \left(1-\frac{J_i}{L_i}+\ln\frac{J_i}{L_i}\right)

.. math:: \ln\gamma_i^R = q_i \left(1-\ln s_i - \sum_j \theta_j\frac{\tau_{ij}}{s_j}\right)


.. code-block:: python

	gamma_C = lambda Ji,qi,Li: exp(1 -Ji+log(Ji)-5*qi*(1-Ji/Li+log(Ji/Li)))
	gamma_R = lambda qi,si,thetai,thetaj,tauij,tauji,sj: exp(qi *(1-log(abs(si-(thetai*tauij/si+thetaj*tauji/sj)))))
	
	#summary of chunk
	
	#summary of chunk


wobei die Koeffizienten folgendermaßen berechnet werden:

.. math:: J_i = \frac{r_i}{\sum_j r_j x_j}

.. math:: L_i \frac{q_i}{\sum_jq_j x_j}

.. math:: s_i = \sum_l \theta_l \tau_{li}


.. code-block:: python

	s_1 = theta_2 * tau_21
	s_2 = theta_1 * tau_12
	gamma_R_1 = gamma_R(q_1,s_1,theta_1,theta_2,tau_12,tau_21,s_2)
	
	#summary of chunk
	
	gamma_R_1 = 2.6151
	s_2 = 0.32425
	s_1 = 0.47018
	#summary of chunk


Überflüssig Excess Enthalpie nicht benötigt
===========================================
Nach der UNIQUAC Methode setzt sich die excess enthalpie aus einen kombinatorischen und einem residualen Term zusammen zu:

.. math:: g^E = g^C + g^R

wobei diese mit folgenden Faktoren berechnet werden können :num:`figuretest`:

.. math:: g^C = \sum_i x_i \ln\frac{\phi_i}{x_i} + 5\sum_i q_i x_i\ln\frac{\theta_i}{\phi_i}

.. math:: g^R = - \sum_i q_i x_i \ln\left(\sum_j \theta_j\tau_{ji}\right)


.. code-block:: python

	gC = x_1 * log(phi_1/x_1) + x_2 * log(phi_2/x_2) + \
	     5 *( q_1 * x_1 * log(theta_1/phi_1) + q_2 *x_2 * log(theta_2/phi_2) )
	
	gR = - (q_1*x_1*log(theta_1*tau_12+theta_2*tau_21)+ \
	        q_2*x_2*log(theta_1*tau_12+theta_2*tau_21))
	
	#summary of chunk
	
	gC = -0.91428
	gR = 0.35536
	#summary of chunk


.. [SVNA] Introduction to Chemical Thermodynamics, Smith VanNess Abott
