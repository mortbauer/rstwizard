#!/usr/bin/env python
# coding=utf-8

from rstwizard import sphinxapp


def main(argv=None):
    rstwizard = sphinxapp.SphinxFrontend(sys.argv[1:])
    rstwizard.main()

if __name__ == '__main__':
    main()
