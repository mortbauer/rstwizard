import os
from docutils import nodes
from docutils.parsers.rst.directives import unchanged
from docutils.parsers.rst import directives

from sphinx.util.compat import Directive

class cite(nodes.Inline, nodes.TextElement):

    @staticmethod
    def visit_node(translator, node):
        if node.children:
            if translator.builder.name == 'mylatex':
                translator.body.append(u"\\%s{" % node.tagname)

    @staticmethod
    def depart_node(translator, node):
        if node.children:
            if translator.builder.name == 'mylatex':
                translator.body.append(u"} ")

class bibliographyDirective(Directive):

    has_content = False
    required_arguments = 1
    option_spec = {'style': unchanged}

    def run(self):
        style = self.options.get('style','plain')
        bibdb = [ os.path.splitext(x)[0] for x in self.arguments]
        self.state.document.settings.env.config.latex_additional_files.extend(self.arguments)
        self.state.document.settings.env.config.bibtex_style = style
        self.state.document.settings.env.config.bibtex_files = ','.join(bibdb)
        return []

def setup(app):
    app.add_config_value('bitex_style','plain','latex')
    app.add_config_value('bitex_files', '','latex')
    app.add_node(cite,
                 latex=(cite.visit_node, cite.depart_node))
    app.add_generic_role('cite', cite)
    app.add_directive('bibliography', bibliographyDirective)

