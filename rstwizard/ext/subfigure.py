"""
Adds subfigure functionality
"""
import re
from docutils import nodes
from docutils.parsers.rst import directives
from docutils.parsers.rst.directives.images import Figure
from sphinx.util.compat import Directive

class subfigures(nodes.General, nodes.Element): pass
class subfigure(nodes.figure): pass

def visit_subfigure_tex(self, node):
    ids = ''
    if not self.elements['listoffigures']:
        self.elements['listoffigures'] = '\\listoffigures'
    for id in self.next_figure_ids:
        ids += self.hypertarget(id, anchor=False)
    self.next_figure_ids.clear()
    if (not 'align' in node.attributes or
        node.attributes['align'] == 'center'):
        # centering does not add vertical space like center.
        align = '\n\\centering'
        align_end = ''
    else:
        # TODO non vertical space for other alignments.
        align = '\\begin{flush%s}' % node.attributes['align']
        align_end = '\\end{flush%s}' % node.attributes['align']
    self.body.append('\\begin{subfigure}[htbp]{%(width)s}%(alignment)s\n' % 
            {'width':node['width']+'\\textwidth','alignment':align})
    if any(isinstance(child, nodes.caption) for child in node):
        self.body.append('\\capstart')
    self.context.append(ids + align_end + '\n\\end{subfigure}\n')

def depart_subfigure_tex(self, node):
    self.body.append(self.context.pop())


class SubFigures(Directive):
    has_content = True
    optional_arguments = 3
    final_argument_whitespace = True
    subfigurewidth = re.compile('[0-9]*(?=%)')

    option_spec = {'label': directives.uri,
                   'alt': directives.unchanged,
                   'width': directives.unchanged_required}
    
    def run(self):
        label = self.options.get('label', None)
        width = self.options.get('width', None)
        alt = self.options.get('alt', None)
        
        node = nodes.figure('', ids=[label] if label is not None else [])
        
        if width:
            node['width'] = width
        else:
            node['width'] = '100%'
        if alt:
            node['alt'] = alt
        else:
            node['alt'] = ''
        
        if self.content:
            anon = nodes.Element()
            subfigs = nodes.Element()
            self.state.nested_parse(self.content, self.content_offset, anon)
            nsubfigs = 0
            for anonnode in anon:
                if isinstance(anonnode,nodes.figure):
                    nsubfigs += 1
                    subfig = subfigure()
                    subfig.tagname = 'figure' # easyway to add it to the labelel of the std domain
                    subfig += anonnode.children
                    if isinstance(subfig[0],nodes.image):
                        subwidth = subfig[0]['width']
                        subfig[0]['width'] = '100%'
                        if not subwidth:
                            subwidth = 1.0 / nsubfigs
                        else:
                            subwidth = float(self.subfigurewidth.match(subwidth).group())/100
                    else:
                        subwidth = 1
                    width = float(self.subfigurewidth.match(node['width']).group())/100
                    subfig['width'] = str(width * subwidth)
                    subfigs.append(subfig)
                elif isinstance(anonnode,nodes.target):
                    subfigs.append(anonnode)
            node.extend(subfigs) 

        if label is not None:
            targetnode = nodes.target('', '', ids=[label])
            node.append(targetnode)

        if self.arguments:
            node.append(nodes.caption('',' '.join(self.arguments)))

        return [node]

class numref(nodes.reference): pass

def get_subfigureref(app, env, node, contnode):
    """ almost ident to the std doamin resolve xref function of sphinx"""
    if node['refexplicit']:
        # reference to anonymous label; the reference uses
        # the supplied link caption
        docname, labelid = env.domaindata['std']['anonlabels'].get(
                node['reftarget'], ('',''))
        sectname = node.astext()
    else:
        # reference to named label; the final node will
        # contain the section name after the label
        docname, labelid, sectname = env.domaindata['std']['labels'].get(
                node['reftarget'],('','',''))
    if not docname:
        return None

    newnode = numref('', '', internal=True)
    innernode = nodes.emphasis(sectname, sectname)
    newnode['refdoc'] = node['refdoc']
    newnode['refuri'] = app.builder.get_relative_uri(node['refdoc'], docname)
    newnode['refuri'] += '#' + labelid
    newnode.append(innernode)
    return newnode

def setup(app):
    app.add_directive('subfigures', SubFigures)
    app.add_node(subfigure,
                 latex=(visit_subfigure_tex, depart_subfigure_tex))
    app.connect('missing-reference',get_subfigureref)


